import React, { Component } from "react";
import Allproducts from "./component/Allproducts/Allproducts";
import Navbar from "./component/Navbar/Navbar";

class App extends Component {
  render() {
    return (
      <div>
        <Navbar />
        <Allproducts />
      </div>
    );
  }
}

export default App;
