import React, { Component } from "react";
import Productcard from "../Productcard/Productcard";
import "./Allproducts.css";
class Allproducts extends Component {
  constructor(props) {
    super(props);

    this.API_STATES = {
      LOADING: "loading",

      LOADED: "loaded",

      ERROR: "error",
    };

    this.state = {
      totalProducts: [],

      status: this.API_STATES.LOADING,

      errorMessage: "",
    };

    this.url = "https://fakestoreapi.com/products";
  }

  fetchData = (url) => {
    this.setState(
      {
        status: this.API_STATES.LOADING,
      },
      () => {
        fetch(url)
          .then((response) => response.json())
          .then((data) => {
            this.setState({
              status: this.API_STATES.LOADED,
              totalProducts: data,
            });
          })
          .catch((error) => {
            this.setState({
              status: this.API_STATES.ERROR,
              errorMessage:
                "An API error occurred. Please try again in a few minutes.",
            });
          });
      }
    );
  };

  componentDidMount = () => {
    this.fetchData(this.url);
  };

  render() {

    let AllData = this.state.totalProducts;

    const allProducts = AllData.map((product) => {
      return <Productcard key={product.id} data={product} />;
    });

    return (
      <>
        {this.state.status === this.API_STATES.LOADING && (
          <div className="loader"></div>
        )}

        {this.state.status === this.API_STATES.ERROR && (
          <div className="error">
            <h1>{this.state.errorMessage}</h1>
          </div>
        )}

        {this.state.status === this.API_STATES.LOADED &&
          AllData.length === 0 && (
            <div className="error">
              <h1>
                No products available at the moment. Please try again later.
              </h1>
            </div>
          )}
        {this.state.status === this.API_STATES.LOADED && (
          <div className="allProducts">{allProducts}</div>
        )}
      </>
    );
  }
}

export default Allproducts;
